
Page({
  data: {
    componentLists:[
      {
        path: '/pages/demo-loading/index',
        name: '加载中'
      },
      {
        path: '/pages/demo-pullup-modal/index',
        name: '上拉弹窗'
      },
      {
        path: '/pages/demo-side-modal/index',
        name: '侧边弹窗'
      },
      {
          path: '/pages/demo-tab/index',
          name: '标签页'
      }
    ],
    funclist: [
      {
        path: '/pages/scrollLockArea/index',
        name: '页面滚动跟随区域'
      }
    ]
  }
})
