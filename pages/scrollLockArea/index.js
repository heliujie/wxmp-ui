
// 滚动设置相关默认配置数值
const SCROLL_MORE_DISTANCE = -40
const FIRST_HOMEUNIT_MORE = 80
const NOFIRST_HOMEUNIT_MORE = 88
const SPACE_DISTANCE = 24

let windowWidth = 0
let windowHeight = 0
let pageWrapHeight = 0

Page({
  data: {
    list: [],
    fixedHeaderItemlistMap: {},
    currentFixedHeaderIdx: 0,
    fixedheaderFlag: false,
    fixedHeaderBoxWidth: 9000,
    hunitFixedHeaderScrollLeftValue: 0,
    hasHomeUnitOffsetValue: false,
    fixedHeaderTranslateX: 0,
    fixedHeaderTranslateWidth: 0
  },
  onLoad() {
    this.listdata = [{title:'测试项A'},{title:'测试项B'},{title:'测试项C'},{title:'测试项D'},{title:'测试项E'},{title:'测试项F'},{title:'测试项J'},{title:'测试项H'}]
    setTimeout(() => {
      this.setData({
        list: this.listdata
      }, this.afterFinishedRender)
    }, 1000)
  },
  _getDomBoundingClientRect(id){
    return wx.createSelectorQuery().select(id).boundingClientRect()
  },
  // 监听页面滚动事件
  onPageScroll: function(e){
    const self = this
    // 重新检查
    const checkAgain = () => {
      // 如果没有获取到offsetValue
      if (!this.data.hasHomeUnitOffsetValue) {
        this.data.fixedheaderFlag && this.setData({ fixedheaderFlag: false })
        return
      }
      // 获取到offsetTop
      const firstTop = this.listdata[0].nativeOffsetTopValue

      // 解决滚动到底部往上拖动问题
      if (Math.abs(pageWrapHeight - e.scrollTop) <= windowHeight - 40) {
        this.data.fixedheaderFlag && this.setData({ fixedheaderFlag: false })
        return
      }

      // 控制是否显示下拉固定头部
      if (e.scrollTop > firstTop) {
        !this.data.fixedheaderFlag && this.setData({ fixedheaderFlag: true })

        // 控制在对应区间，切换当前选项
        const cidx = this._getScrollAreaIdx(this.listdata, e.scrollTop)
        if (this.data.currentFixedHeaderIdx === cidx) return

        this.setData({
          currentFixedHeaderIdx: cidx === -1 ? this.data.currentFixedHeaderIdx : cidx
        })

        const { width, offsetLeft } = this.data.fixedHeaderItemlistMap[this.data.currentFixedHeaderIdx]
        this.setData({
          fixedHeaderTranslateX: offsetLeft,
          fixedHeaderTranslateWidth: width
        })

        // 避免重复调用getSystemInfoSync去获取windowWidth
        windowWidth === 0 ? (windowWidth = wx.getSystemInfoSync().windowWidth) : null
        // 判断是否不在可视区
        const compareValue = (reactObj) => {
          const { left, width } = reactObj
          const widthDistance = parseInt(left + width)
          if (widthDistance >= windowWidth) {
            self.setData({
              hunitFixedHeaderScrollLeftValue: self.data.hunitFixedHeaderScrollLeftValue + widthDistance - windowWidth + SPACE_DISTANCE
            })
          }
          if (parseInt(left) <= SPACE_DISTANCE) {
            self.setData({
              hunitFixedHeaderScrollLeftValue: self.data.hunitFixedHeaderScrollLeftValue - Math.abs(parseInt(left)) - SPACE_DISTANCE
            })
          }
        }
        // 解决滚动过程中，下拉固定头部当前项目不在可视区范围内的问题
        this
          ._getDomBoundingClientRect(`#hunitItem${this.data.currentFixedHeaderIdx}`)
          .exec(res => {
            if (res[0] == null) return
            compareValue({
              left: res[0].left,
              width: res[0].width
            })
          })
      } else if (e.scrollTop <= firstTop) {
        this.setData({ fixedheaderFlag: false })
      }
    }
    checkAgain()
  },
  switchFixedHeader(e) {
    const selectedIdx = e.currentTarget.dataset['idx']
    const self = this
    if (this.data.currentFixedHeaderIdx === selectedIdx) return
    const scrollTop = self.listdata[selectedIdx].nativeOffsetTopValue + (this.data.hasCards ? (selectedIdx === 0 ? 0 : -SCROLL_MORE_DISTANCE) : SCROLL_MORE_DISTANCE)

    wx.pageScrollTo({
      scrollTop: scrollTop,
      duration: 0
    })

    // 下拉线指向移动
    const { width, offsetLeft} = this.data.fixedHeaderItemlistMap[selectedIdx]
    this.setData({
      fixedHeaderTranslateX: offsetLeft,
      fixedHeaderTranslateWidth: width
    })
  },
  _getScrollAreaIdx(homeUnitList, currentScrollTop){
    // 转换成 [[a, b], [b, c], [c, d], [d, d+1]]
    const middelMap = []
    let idx = -1
    homeUnitList.forEach((item, idx) => {
      middelMap.push([
        item.offsetTop,
        !!homeUnitList[idx + 1] ? homeUnitList[idx + 1].offsetTop : item.offsetTop + 1
      ])
    })

    // 判断
    middelMap.forEach((areaItem, areaIdx) => {
      if (areaIdx < middelMap.length - 1) {
        if (currentScrollTop > areaItem[0] && currentScrollTop <= areaItem[1]) {
          idx = areaIdx
        }
      } else {
        if (currentScrollTop > areaItem[0]) {
          idx = areaIdx
        }
      }
    })
    return idx
  },
  afterFinishedRender() {
    // 40是每一个的margin-right值
    let _w = (this.data.list.length - 1) * 40
    this.listdata.forEach((item, index) => {
      if (index === 0) {
        this._getDomBoundingClientRect('#homeUnitTopBox').exec(res => {
          if (res[0] == null) return
          item.nativeOffsetTopValue = res[0].height
          item.offsetTop = res[0].height - FIRST_HOMEUNIT_MORE
        })
      } else {
        this._getDomBoundingClientRect(`#hunitBox${index - 1}`).exec(res => {
          if (res[0] == null) return
          item.nativeOffsetTopValue = (this.listdata[index - 1]['nativeOffsetTopValue'] + res[0].height)
          item.offsetTop = (this.listdata[index - 1]['nativeOffsetTopValue'] + res[0].height) - NOFIRST_HOMEUNIT_MORE

          // 意味着做完了所有工作，可以更改状态
          if (index === this.data.list.length - 1) {
            // 更改状态
            this.setData({
              hasHomeUnitOffsetValue: true
            })
          }
        })
      }

      // 计算fixedHeaderBoxWidth、fixedHeaderItemlistMap
      this._getDomBoundingClientRect(`#hunitItem${index}`).exec(res => {
        if (res[0] == null) return
        // fixedHeaderItemlistMap
        this.data.fixedHeaderItemlistMap[index] = {
          width: parseInt(res[0].width),
          offsetLeft: (() => {
            let w = 0
            for (let i = 0; i < index; i++) {
              // 40是margin值
              w += this.data.fixedHeaderItemlistMap[i].width + 40
            }
            return parseInt(w)
          })()

        }
        // 默认宽度
        if (index === 0){
          this.setData({
            fixedHeaderTranslateWidth: parseInt(res[0].width)
          })
        }

        // fixedHeaderBoxWidth
        _w += res[0].width
        if (index === this.listdata.length - 1) {
          this.setData({
            fixedHeaderBoxWidth: Math.ceil(_w)
          })
        }
      })
    })

    this._getDomBoundingClientRect('#pageWrap').exec(res => {
      if (res[0] == null) return
      pageWrapHeight = res[0].height
    })
  }
})
