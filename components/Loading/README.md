##  Loading

### 使用指南

#### 代码演示
**index.json**
```Javascript
  "usingComponents": {
    "component-loading": "/components/Loading/loading"
  }
```

**index.wxml**

```Javascript
<component-loading  id="J_loading" ltype="wave" wave-position-class="loading-wave-position"></component-loading>
```

**index.js**

```Javascript
 onLoad() {
    this.$Loading = this.selectComponent("#J_loading")
    setTimeout(() => {
      this.$Loading.showLoading()
    }, 2000)
 }
```

效果

![](data:image/gif;base64,R0lGODlhdgFwAncAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQADwAAACwAAAAAdgFwAoQAAAAgICBAQEBgYGB/f3+BaEyLc1mOeF6PeGCTfmaTf2iUgGmfn5+xopG/v7/BtafBtqjY0Mjf39/k49/q5uHt6ubz8e718/H+/v4AAAAAAAAAAAAAAAAAAAAAAAAAAAAF/+CEjWRpnmiqrmzrvnAsz3Rt33iu77fI/8CgcEgsGo/Imi/JbDqf0KiUuZxar9isdsurcr/gsHh89JLP6LRabV673/A4si2v2+94Fj3P7/uDEg4MDkB7f4eIiSwEAQCOjgEEO4aKlZZ+EgSQAgMCjwI6lJejpG8SnpEmDo6gOKKlsLFhmgESKKcAkj2yvL1jEgABhCmrAMNKvsnKLZnBukGaAyQMAgECxwO5u8vcvpkMJLif4QzPN57DwI8AthgMANI2r930mI/xxevgGJ6OOY3t8jnah8GfvHoIR717ZEvTOm3qBuIAOGLho3TBtiXciMhigIoPjY1o5OhYjWwE1/8JG7GqFTKOMP/QetYPEgkHAVLhiHaz2jUS2czNmBezaJxmOoMUM2kC2MqDRqNKLaHJZVNP8aBO3WoUFVMGjT664ko2pjhrnT6101i2LUKLj5KOdUsXYaBBherq3buCKN+/Uv0CHgxTMOHD9QwjXqxMMePHshxDnjxKMuXLiSxj3txHM+fPdjyDHv1GNOnTaEyjXh1GNevXWlzDnj1FNu3bTmzj3l2Gt+80un8L7zK8+JfgxpMPVc68dvPnT5BDn25COvXr1q9Pz679OffuzL+DTy5+fPHy5oWjT+97Pfvd7t/fji9/Nv36r+/jX61//+n+/o0GYICfDUjgZgYeeFn/ggpOxmCDjz0I4WISTnhYhRYOhmGGf23I4V4eflhXiCK6RWKJZZ2IIlcqrjhViy5GBWOMRc1IY2E3amdjjgntyGNiP24XJHQ+DrlMkUYmg2SSvSzJZGRPkhelcU5OSUqVVl6CZZaVbMllZl+2FyZvXo7pR5lm8oFmmnisyWZob9oXJ2xuzglHnXa6gWeebPCJ2p5+ngFooGMMSmhrh4JmaKJcLMpobI8iGClmjk5qRaWWSoFpplBsymlun0YYKmOejjqHqYiVimoRqq46RKuuBgFrrD/MSuskt/Jla65z8UrXrr6+FGxbwA4rQ7HGwoBssi4sy6wez7IY7VbOTotC/7XWVpetjNsahW23GHzbrbjbkputudaiO6260bL7rLvMwpusvMbSO6y9weLrq7688purv7cCTKvAsRLsqsGrIoyqwqYyPKrDoUL8qcScUpypxZZiPKnGkXL8qMeMgpyoyIeSTKjJgaLsp8p8spyny3bCPKfMcdL8ps1s4pymzmbyPKbPYQL9pdBcEp2l0VYiPaXSUTL9pNNMQp2k1EZSPaTVQWL9o9Y8cp2j1zeCTaPYMZLtotkroo2i2iWyLaLbH8LNodwZ0m2h3RPiDaHeDfKtoN8HAk6g4AES7p/h+yGOn+L1MS6f4+9Bzp7k6VFunuXjYQ6e5t1xriO4MXmOHf/oOJK+kejUoS6k6T2y3rrrQMJOj+pEyj677d3Q7h3u3OjenO/h8X6k8I0Rr6TxvgCvnPJSIs8L81Q6/7z0UFIPC/TnWX+99qVgP5z36nF/pfiVka+l+ZaA/5v6YqKvCPtkuv++/GDSfwj88Nl/v/5/4I+b//Ph35kE2BkCqsmAeQAgbRQoJwTegYF0cuADJQgnCsoBgvmx4AU1GAcMssaD/OHgnURYGhLqyYRrAOGfUNgnFqbGhS+EIRlU+B8ZztCGhcKhGGhIGh4KSIeIAuJxhDhEIm7Bh4oy4hGVCCkmYgGJBXLiE6V4BShyxoqSoqJztKgpLnbRi50CYxjFCCr/MjYBi5Qy4xnVSAU2JkFvF6gABShQgQskzj8XiEADElCAAiSgARGw4+P2Y4EHHKCPiCzAAR5ggUHW5wIPMEAiE2mABwiycsq5wCVdEIFDTjKRB4hA5JLDxwIo4AUXaMAnP9mAURanlH1MgAsqAMtVxtKVwrnAJ11AAVvucnLF0eUkeenLYQKzOApI5AJmWUtbyvKYr+zjMl2QymL2sZXQfE8ni4kAUWaTPZCU5CoNAIFNZo6QhvwkAiDQSFw+Uo+l/GMgG+fGU9XTCGhc0D3xuU9W9ZMI+aRMQB30z1cVVAgDhUxCRXXQvDS0Vg+FaERxNdFQVNSiF+1VRoW1URos/5RUHdVKSI81Uo+WdDknjcFHKZRSlbZUWS99wUpTFdNm1bQFM73QTaG1UxXklDA/1VBPfTrUFAQVMEftUFGvtdQTJFVXTdVWVEnwVBBNlapXHUFV9bLVEWU1XF/t6q/CStasitVEZb3qWYmV1qmuNUVtjepbyTJXaZk1rk2tK7XwulS9voivRfVrYAA7VMFy666IVSthe2pYby12p42t0WNvGtnQTbamlS2dYhPr1svGNLMcAe3pPPtS0b5us6jtLGflStqWmhYhr41dalm72ry2NqWxvV1t+3rbk+Y2d70t6W97F9yRDnd4uw1scUN63OIlt7DL7Whzj/dcxkZ3o8XTTd51M5rdJm33ot2dXnUh+92Khrd6s7XteClb3omeNxbv3d56MdveiMa3e/V96H3HN9/P5reh+y1ff0v734MG+HwDdm2BC3rg9C34nw3u0oP7GeH5JRi3E95nheuXXt5e2LcZvueGETHi/X1YuCGuZ4n7l2I3rniAJzZui9n44gLGmLkzVmOND3hj6ebYjDtO4I/JGOQ2DVmMRZ5gj7F7ZDAmuYIdVu6SudtkLz65Dlfe4JTBW2UuZrmDXdbil0e45YqGAAAh+QQADgAAACyhAFoBOwAPAISBaEyLc1mOeF6PeGCTfmaTf2iUgGmciHKxopHBtafBtqjY0Mjq5uHt6ubz8e718/H+/v4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFiSAkjmRpnmhKPg3DNI8qz3T5LAgBAASyxLWgUORICHZIgCDhGDplj0QgmQwkgE/TA0tbHKlJwSJr0gEKtQcCDEaQSeYdgdaIs+Wjw+H5ANMYd34ienyCMoCBSW8jBUkGdHZ3c4siZo80aok7bpRZXokDY518UncBClyjQkVfSQMKTapZNzlyPkAhACH5BAABAAAALKQAWgE4AA8AgoFoTJN/Z5N/aJSAaZyIcvXz8f7+/gAAAAMyaLrc/jDKSau9OOvNu99F8Y1OAAACqZonEKgKQYBtCxsyXQO3J9SDnocVFBqPyKQSmQAAOw==)

#### `Loading` 支持的具体参数如下
| 参数       | 说明      | 类型       | 默认值       | 其他      |
|-----------|-----------|-----------|-------------|-------------|
| ltype | 类型 | String | 'icon' | 'icon'或'wave' |
| wave-position-class | 'wave'类型有效 | String | / | |
| icon-class | 'icon'类型有效 | String | / | |