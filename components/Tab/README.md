##  Tab

### 使用指南

#### 代码演示
**index.json**
```javascript
  'usingComponents': {
    'com-tab': '/components/Tab/tab'
  }
```

**index.wxml**
```html
<com-tab
    tabList='{{ navlist }}'
    my-tab-nav='star-score-tab-nav'
    bind:tapTabItemEvent='tapTabItemEvent'>
</com-tab>
```


**index.js**
```Javascript
tapTabItemEvent(e) {
    console.log(e.detail.o)
}
```

#### 描述
- 只负责标签页切换(内部一个状态的更改), 切换标签页对应的内容展示自行根据回调函数返回值灵活处理

#### `Tab` 支持的具体参数如下
| 参数       | 说明      | 类型       | 默认值       |
|-----------|-----------|-----------|-------------|
| tabList | 标签页数据 | Array | [] |  |
| my-tab-nav | 自定义样式类 | String |  |
| tapTabItemEvent | tap标签页触发;返回值包括当前索引值和当前对象 | Event |  |