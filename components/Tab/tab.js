
Component({
    externalClasses: ['my-tab-nav'],
    options: {

    },
    /**
     * 组件的属性列表
     * 用于组件自定义设置
     */
    properties: {
        tabList: {
            type: Array,
            value: []
        }
    },

    /**
     * 私有数据,组件的初始数据
     * 可用于模版渲染
     */
    data: {
        activeIdx: 0
    },

    /**
     * 组件的方法列表
     * 更新属性和数据的方法与更新页面数据的方法类似
     */
    methods: {
        /*
         * 公有方法
         */

        /*
        * 内部私有方法建议以下划线开头
        * triggerEvent 用于触发事件
        */
        _onClickTabItem(e) {
            const idx = e.currentTarget.dataset['index']
            this.setData({activeIdx: idx})
            //触发取消回调
            this.triggerEvent("tapTabItemEvent", {
                index: idx,
                o: this.data.tabList[idx]
            })
        }
    }
})
